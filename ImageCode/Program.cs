﻿using QRCoder;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageCode
{
    class Program
    {
        static void Main(string[] args)
        {
            // 二维码生成
            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode("The text which should be encoded.", QRCodeGenerator.ECCLevel.Q);
            QRCode qrCode = new QRCode(qrCodeData);
            Bitmap qrCodeImage = qrCode.GetGraphic(20);

            // 图片文字
            Bitmap fontImage = new Bitmap(110,30);
            Graphics fontG = Graphics.FromImage(fontImage);
            Font fontForm = new Font("Arial", 21);
            SolidBrush fontColor = new SolidBrush(Color.White);
            fontG.Clear(System.Drawing.Color.FromArgb(3,4,8));
            fontG.DrawString("888888", fontForm, fontColor,0,0);

            // 图像大小设置
            Bitmap newQrCodeImage = new Bitmap(qrCodeImage, 277, 277);

            // 文件名
            var imgName = DateTime.Now.ToString().Replace(" ", "").Replace(":", "").Replace("/", "");

            // 背景图
            Bitmap icon = new Bitmap("../../Image/IMG_4012.PNG");
            Graphics g = Graphics.FromImage(icon);

            // 合成
            g.DrawImage(newQrCodeImage, 209,804, newQrCodeImage.Width, newQrCodeImage.Height);
            g.DrawImage(fontImage, 333, 730, fontImage.Width, fontImage.Height);

            // 另存为文件
            icon.Save("../../Image/" + imgName + ".jpg", ImageFormat.Jpeg);
        }


    }
}
